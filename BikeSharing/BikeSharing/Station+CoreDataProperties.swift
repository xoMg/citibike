//
//  Station+CoreDataProperties.swift
//  BikeSharing
//
//  Created by Alec Montgomery on 7/24/16.
//  Copyright © 2016 Alec Montgomery. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Station {

    @NSManaged var address: String?
    @NSManaged var capacity: NSNumber?
    @NSManaged var crossStreet: String?
    @NSManaged var latitude: NSNumber?
    @NSManaged var longitude: NSNumber?
    @NSManaged var name: String?
    @NSManaged var postCode: String?
    @NSManaged var shortName: String?
    @NSManaged var stationID: String?
    @NSManaged var bikesAvailable: NSNumber
    @NSManaged var bikesDisabled: NSNumber
    @NSManaged var docksAvailable: NSNumber
    @NSManaged var docksDisabled: NSNumber
    @NSManaged var isInstalled: NSNumber?
    @NSManaged var isRenting: NSNumber?
    @NSManaged var isReturning: NSNumber?
    @NSManaged var lastReported: String?

}
