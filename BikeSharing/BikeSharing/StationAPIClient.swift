//
//  MyAPIClient.swift
//  BikeSharing
//
//  Created by Alec Montgomery on 7/25/16.
//  Copyright © 2016 Alec Montgomery. All rights reserved.
//

import Foundation
import CoreData

class StationAPIClient {
    
    /** Performs and parses request for the station information endpoint, which contains station identifying data such as name, address
     *  etc.
     */
    class func getStationInformation(completion:APIDefaultRequestCompletion) {
        
        APIClient.performDefaultRequestWithEndpoint("station_information.json") { (dictionary, error) in
            
            // insert new objects..
            if let dict = dictionary {
                Station.updateObjectsForJSONResponse(dict, insert: true)
            }
            
            completion(dictionary: dictionary, error: error)
        }
    }
    
    /** Performs and parses request for the station status endpoint, which contains station status info like current number of bikes,
     *  isRenting, number of bikes available etc. In practice this should be called more often than stationInformation()
     */
    class func getStationStatus(completion:APIDefaultRequestCompletion) {
        
        APIClient.performDefaultRequestWithEndpoint("station_status.json") { (dictionary, error) in
            
            // don't insert new objects, just refresh 
            if let dict = dictionary {
                Station.updateObjectsForJSONResponse(dict, insert: false)
            }
            
            completion(dictionary: dictionary, error: error)
        }
    }
}
