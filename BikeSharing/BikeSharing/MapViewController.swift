//
//  ViewController.swift
//  BikeSharing
//
//  Created by Alec Montgomery on 7/22/16.
//  Copyright © 2016 Alec Montgomery. All rights reserved.
//

import UIKit
import MapKit
import CoreData
import AMPopTip


class MapViewController: UIViewController, MKMapViewDelegate {
    
    @IBOutlet var titleLabel : UILabel!
    
    @IBOutlet var mapView : MKMapView!
    @IBOutlet var topBackgroundView : UIView!
    
    @IBOutlet var refreshButton : UIButton!

    /** Popover shown if annotations are selected
     */
    var popTip = AMPopTip()
    
    // MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self
        
        popTip.popoverColor = UIColor.whiteColor()
        popTip.textColor = UIColor.blackColor()
        
        if Reachability.isConnectedToNetwork() {
            
            mapView.setRegion(kAppDefaultCoordianteRegion, animated: true)
            
            SystemAPIClient.getSystemInformation({ (dictionary, error) in
                self.reloadSystemFromStore()
            })
            refreshMapView()
        } else {
         
            self.title = "Map"
            
            // load from any available local store.
            self.reloadAnnotations()
        }
    }
    
    @IBAction func refreshMapView() {
        
        func getStationStatus() {
            StationAPIClient.getStationStatus({ (dictionary, error) in
                self.reloadAnnotations()
            })
        }
        
        // check if we should get stations as well or just station status items.
        if Station.allItems()?.count > 0 {
            /** Alternative approach would be getting current annotations and updating the associated Station Status information
             */
            getStationStatus()
        } else {
            StationAPIClient.getStationInformation({ (dictionary, error) in
                getStationStatus()
            })
        }
    }
    
    private func reloadSystemFromStore() {
        if let systemItem = System.allItems()?.first {
            if let system = systemItem as? System {
                self.title = system.name
                
                if let systemID = system.systemID {
                    self.titleLabel.text = systemID
                }
            }
        }
    }
    
    private func reloadAnnotations() {
    
        /**  TODO: Possible improvement would be a mapping library that shows an overlay with more dense areas color-coded in, as
         *   opposed to individual pins.
         */
        
        mapView.removeAnnotations(mapView.annotations)
        
        let allStations = Station.allItems() as! [Station]
        
        // for all station items, setup map annotations
        for station in allStations {
            let latitude = station.latitude?.doubleValue
            let longitude = station.longitude?.doubleValue
           
            if let lat = latitude, let long = longitude {
    
                let coordinate =  CLLocationCoordinate2DMake(lat, long)
                let mapAnnotation = StationAnnotation(coordinate: coordinate, title: "thing", subtitle: nil)
                
                mapAnnotation.station = station
                mapAnnotation.numberOfBikesAvailable = station.bikesAvailable.integerValue
                mapAnnotation.numberOfDocksAvailable = station.docksAvailable.integerValue
                
                mapView.addAnnotation(mapAnnotation)
            }
        }
    }
}

let MapAnnotationIdentifier = "AnnotationIdentifier"

extension MapViewController {

    func mapView(mapView: MKMapView, didSelectAnnotationView view: MKAnnotationView) {
       
        mapView.deselectAnnotation(view.annotation, animated: true)
        
        // hide any current popup
        self.popTip.hide()
        
        if let annotation = view.annotation as? StationAnnotation {
            if let station = annotation.station {
                let isRenting = station.isRenting!.boolValue ? "is" : "is not"
                let isRentingConcententation = "Station \(isRenting) renting"
                let popoverText = "\(station.name!) \n Number of bikes Available: \(station.bikesAvailable) Number of Docks Available: \(station.docksAvailable) \n \(isRentingConcententation) "
                
                let fromViewFrame = mapView.convertRect(view.frame, toView: self.view)
            
                self.popTip.showText(popoverText, direction: .Up, maxWidth: 200, inView: self.view, fromFrame: fromViewFrame)
            }
        }
    }
    
    func mapViewDidFinishLoadingMap(mapView: MKMapView) {
        
        // load any local items before the request finishes.
        
        self.reloadSystemFromStore()
        
        // show any Station items currently available.
        self.reloadAnnotations()
    }
    
    func mapView(mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        // hide popup if its shown 
        if self.popTip.isVisible {
            self.popTip.hide()
        }
    }
}
