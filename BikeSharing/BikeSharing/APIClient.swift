//
//  APIClient.swift
//  BikeSharing
//
//  Created by Alec Montgomery on 7/22/16.
//  Copyright © 2016 Alec Montgomery. All rights reserved.
//

import Foundation


private let sharedAPIClient = APIClient()

/* Request completion block, data from NSURLSession dataTask: */
typealias APIRequestCompletion = (data:NSData?, error:NSError?)->()

/* Default request completion block; passes dict serialized JSON response */
typealias APIDefaultRequestCompletion = (dictionary:NSDictionary?, error:NSError?)->()

class APIClient {
    
    let kAPIBaseURL = "https://gbfs.citibikenyc.com/gbfs"
    let kAPILanguge = "en"
    
    class var sharedInstance : APIClient {
        return sharedAPIClient
    }
    
    private var session : NSURLSession!
    
    init() {
        session = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())
    }
    
    func baseString() -> String {
        return "\(kAPIBaseURL)/\(kAPILanguge)/"
    }
    
    func request(request:APIRequest, completion:APIRequestCompletion) {
        
        guard let endpoint = request.endpoint else {
            return
        }
        let urlString = "\(self.baseString())\(endpoint)"
        let url = NSURL(string: urlString)
        
        let urlRequest = NSMutableURLRequest(URL: url!, cachePolicy: .UseProtocolCachePolicy, timeoutInterval: 60.0)
        if let httpMethod = request.httpMethod?.string() {
            urlRequest.HTTPMethod = httpMethod
        }
        
        var requestError : NSError?
        self.session.dataTaskWithRequest(urlRequest) { (data, urlResponse, error) in
        
            if let httpResponse = urlResponse as? NSHTTPURLResponse {
                
                let status = httpResponse.statusCode
                // 2xx == http status success
                if status < 199 || status > 299 {
                    requestError = (error != nil) ? error : NSError(domain: "com.alecmontgomery.bikesharing", code: status, userInfo: nil)
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), {
                completion(data:data, error:requestError)
            })
        }.resume()
    }

    /** Performs a default request and serializes the JSON data response
     */
    class func performDefaultRequestWithEndpoint(endpoint:String, completion:APIDefaultRequestCompletion) {
        
        let apiRequest = APIRequest(endpoint: endpoint, httpMethod: HTTPMethod.None, params: nil, body: nil)
        
        APIClient.sharedInstance.request(apiRequest) { (data, error) in
            
            guard error == nil else {
                print("Request error = \(error)")
                return
            }
            
            var dictionary : NSDictionary?
            if data != nil {
                do {
                    dictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as? NSDictionary
                }
                catch let JSONError as NSError {
                    return
                }
            }
            
            completion(dictionary: dictionary, error:error)
        }
    }
}
