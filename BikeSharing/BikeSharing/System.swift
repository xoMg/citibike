//
//  System.swift
//  BikeSharing
//
//  Created by Alec Montgomery on 7/23/16.
//  Copyright © 2016 Alec Montgomery. All rights reserved.
//

import Foundation
import CoreData

class System: ManagedObject {
    
    class func updateObjectsForJSONResponse(dictionary:NSDictionary, insert:Bool) {
        
        guard insert == true else {
            NSLog("System objects can only be inserted not updated.")
            return
        }
        
        if let dataDictionary = dictionary["data"] {
         
            guard let entityName = System.entityName() else {
                return
            }
            
            let mainContext = ManagedStack.sharedInstance.managedObjectContext
            
            // create new objects
            if insert {
                
                // delete any current representation of this SystemID
                let uniquingKey = System.uniquingKey()!
                let systemID = dataDictionary[uniquingKey] as! String
                if let managedStation = System.systemForSystemID(systemID) {
                    mainContext.deleteObject(managedStation)
                }
                
                if let managedObject = NSEntityDescription.insertNewObjectForEntityForName(entityName, inManagedObjectContext: mainContext) as? System {
                    
                    managedObject.updateObject(dataDictionary as! NSDictionary)
                }
            }
        }
    }
    
    /** Get a station for some station ID
     */
    class func systemForSystemID(systemID:String) -> System? {
        
        guard let items = System.allItems() else {
            return nil
        }
        guard let systemItems = items as? [System] else {
            return nil
        }
        
        for systemManaged in systemItems {
            if systemID == systemManaged.systemID {
                return systemManaged
            }
        }
        return nil
    }
    
    override class func uniquingKey() -> String? {
        return "system_id"
    }
    
    override class func entityName() -> String? {
        return "System"
    }
    
    override class func remoteToLocalMapping() -> [String : String] {
        return [
                "system_id" : "systemID",
                "name" : "name",
                "start_date": "startDate",
                "short_name": "shortName",
                "operator": "operatorName",
                "url" : "url",
                "purchase_url": "purchaseURL" ,
                "phone_number": "phoneNumber",
                "email" : "email",
                "timezone" : "timezone",
                "language" : "language"
        ]
    }
}
