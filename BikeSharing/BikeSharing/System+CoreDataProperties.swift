//
//  System+CoreDataProperties.swift
//  BikeSharing
//
//  Created by Alec Montgomery on 7/24/16.
//  Copyright © 2016 Alec Montgomery. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension System {

    @NSManaged var email: String?
    @NSManaged var language: String?
    @NSManaged var name: String?
    @NSManaged var operatorName: String?
    @NSManaged var phoneNumber: String?
    @NSManaged var purchaseURL: String?
    @NSManaged var shortName: String?
    @NSManaged var startDate: String?
    @NSManaged var systemID: String?
    @NSManaged var timezone: String?
    @NSManaged var url: String?
    @NSManaged var stations: NSSet?

}
