//
//  String+BikeSharing.swift
//  BikeSharing
//
//  Created by Alec Montgomery on 7/25/16.
//  Copyright © 2016 Alec Montgomery. All rights reserved.
//

import Foundation


extension String {
    func toBool() -> Bool {
        switch self {
        case "True", "true", "yes", "1":
            return true
        case "False", "false", "no", "0":
            return false
        default:
            return false
        }
    }
    
    func toDate() -> NSDate? {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yy-dd-ss"
        return dateFormatter.dateFromString(self)
    }
}
