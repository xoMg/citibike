//
//  BikeSharingConstants.swift
//  BikeSharing
//
//  Created by Alec Montgomery on 7/27/16.
//  Copyright © 2016 Alec Montgomery. All rights reserved.
//

import Foundation
import MapKit

/** New York City
 */
let kAppDefaultCoordianteRegion = MKCoordinateRegion(center:CLLocationCoordinate2DMake(40.71473993, -74.00910629),
                                                  span: MKCoordinateSpanMake(0.05, 0.05))
