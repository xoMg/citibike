//
//  AppDelegate.swift
//  BikeSharing
//
//  Created by Alec Montgomery on 7/22/16.
//  Copyright © 2016 Alec Montgomery. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        return true
    }
    
    func applicationWillTerminate(application: UIApplication) {
        ManagedStack.sharedInstance.saveContext()
    }
}
