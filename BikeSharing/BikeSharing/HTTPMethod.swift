//
//  HTTPMethod.swift
//  BikeSharing
//
//  Created by Alec Montgomery on 7/25/16.
//  Copyright © 2016 Alec Montgomery. All rights reserved.
//

import Foundation

enum HTTPMethod : Int {
    case None, POST, PUT, GET, DELETE
    
    func string() -> String? {
        switch self {
        case .None:
            return nil
        case .PUT:
            return "PUT"
        case .POST:
            return "POST"
        case .GET:
            return "GET"
        case .DELETE:
            return "DELETE"
        }
    }
}
