//
//  Station.swift
//  BikeSharing
//
//  Created by Alec Montgomery on 7/23/16.
//  Copyright © 2016 Alec Montgomery. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class Station: ManagedObject {

    override class func uniquingKey() -> String? {
        return "station_id"
    }
 
    class func updateObjectsForJSONResponse(dictionary:NSDictionary, insert:Bool) {
        
        // unwrap based on api configuration....
        if let dataDictionary = dictionary["data"] {
            if let array = dataDictionary["stations"] as? NSArray  {
                
                guard let entityName = Station.entityName() else {
                    return
                }
                
                // with the JSON data, insert into main store.
                let mainContext = ManagedStack.sharedInstance.managedObjectContext
                
                
                for station in array {
                    
                    if insert {
                        // get object for some ID ; if some ID
                        let uniquingKey = Station.uniquingKey()!
                        let stationID = station[uniquingKey] as! String
                        if let managedStation = Station.stationForStationID(stationID) {
                            mainContext.deleteObject(managedStation)
                        }
                        
                        if let managedObject = NSEntityDescription.insertNewObjectForEntityForName(entityName, inManagedObjectContext: mainContext) as? Station {
                            managedObject.updateObject(station as! NSDictionary)
                        }
                    } else {
                        let stationID = station[Station.uniquingKey()!] as! String
                        
                        // update current objects in the store
                        for stationManaged in Station.allItems()! as! [Station] {
                            if stationID == stationManaged.stationID {
                                stationManaged.updateObject(station as! NSDictionary)
                                break
                            }
                        }
                    }
                }
                
                
                do {
                    // should dispatch notification in which we could reload our table
                    try mainContext.save()
                } catch let error as NSError {
                    print("\(error)")
                }
            }
        }
    }
    
    override class func entityName() -> String? {
        return "Station"
    }
    
    override func updateObject(jsonResponse: NSDictionary) {
        super.updateObject(jsonResponse)
    }
    
    override class func remoteToLocalMapping() -> [String : String] {
    
        return [
        // identifying properties
        "station_id": "stationID",
        "name" : "name",
        "short_name" : "shortName",
        "lat" : "latitude",
        "lon" : "longitude",
        "address" : "address",
        "cross_street" : "crossStreet",
        "post_code" : "postCode",
        "capacity" : "capacity",
        // status properties
        "num_bikes_available": "bikesAvailable",
        "num_bikes_disabled" : "bikesDisabled",
        "num_docks_available": "docksAvailable",
        "num_docks_disabled" : "docksDisabled",
        "is_installed": "isInstalled",
        "is_renting": "isRenting",
        "is_returning": "isReturning",
        "last_reported": "lastReported"]
    }
}

// TODO: make into generic 
extension Station {
    
    /** Get a station for some station ID
     */
    class func stationForStationID(stationID:String) -> Station? {
        
        guard let items = Station.allItems() else {
            return nil
        }
        guard let stationItems = items as? [Station] else {
            return nil
        }
        
        for stationManaged in stationItems {
            if stationID == stationManaged.stationID {
                return stationManaged
            }
        }
        return nil
    }
}
