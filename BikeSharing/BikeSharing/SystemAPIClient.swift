//
//  SystemAPIClient.swift
//  BikeSharing
//
//  Created by Alec Montgomery on 7/29/16.
//  Copyright © 2016 Alec Montgomery. All rights reserved.
//

import Foundation


class SystemAPIClient {
    
    /** Performs request for the system information endpoint, retrieving data for the System object.
     */
    class func getSystemInformation(completion:APIDefaultRequestCompletion) {
        APIClient.performDefaultRequestWithEndpoint("system_information.json") { (dictionary, error) in
            
            if let dict = dictionary {
                // System objects of the same ID are deleted and recreated. 
                System.updateObjectsForJSONResponse(dict, insert: true)
            }
            
            completion(dictionary: dictionary, error: error)
        }
    }
}
