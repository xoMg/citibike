//
//  StationAnnotation.swift
//  BikeSharing
//
//  Created by Alec Montgomery on 7/25/16.
//  Copyright © 2016 Alec Montgomery. All rights reserved.
//

import Foundation
import MapKit

class StationAnnotation: NSObject, MKAnnotation {
    
    var coordinate: CLLocationCoordinate2D
    
    var station : Station?
    
    var title: String?
    var subtitle: String?
    
    var numberOfBikesAvailable:Int = 0
    var numberOfDocksAvailable:Int = 0
    
    var percentageOfBikesAvailable : Float {
        if let capacity = station?.capacity?.integerValue {
            if capacity > 0 {
                return floor(Float(numberOfBikesAvailable)/Float(capacity))
            }
        }
        return 0
    }
    
    var percentageOfDocksAvailable : Float {
        if let capacity = station?.capacity?.integerValue {
            if capacity > 0 {
                return floor(Float(numberOfDocksAvailable)/Float(capacity))
            }
        }
        return 0
    }
    
    init(coordinate: CLLocationCoordinate2D, title: String, subtitle: String?) {
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
    }
    
    
}
