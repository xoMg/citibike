//
//  APIRequest.swift
//  BikeSharing
//
//  Created by Alec Montgomery on 7/25/16.
//  Copyright © 2016 Alec Montgomery. All rights reserved.
//

import Foundation


internal class APIRequest {
    
    var endpoint : String?
    var httpMethod : HTTPMethod?
    var params : [String : String]?
    var body : Dictionary<String, String>?
    
    init(endpoint:String?, httpMethod:HTTPMethod?, params:[String : String]?, body:Dictionary<String, String>?) {
        self.endpoint = endpoint
        self.httpMethod = httpMethod
        self.params = params
        self.body = body
    }
}
