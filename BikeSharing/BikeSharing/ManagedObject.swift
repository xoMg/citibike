//
//  ManagedObject.swift
//  BikeSharing
//
//  Created by Alec Montgomery on 7/25/16.
//  Copyright © 2016 Alec Montgomery. All rights reserved.
//

import Foundation
import CoreData

/**
 */
class ManagedObject : NSManagedObject {
    
    /** Mapping of local to remote property names
     */
    class func remoteToLocalMapping() -> [String : String]? {
        return nil
    }
    
    /** Update item from JSON response
     */
    func updateObject(jsonResponse: NSDictionary) {
        let mapping = self.dynamicType.remoteToLocalMapping()!
        for (key, value) in jsonResponse {
            // if local key exists for this remote key set local value
            if let localKey = mapping[key as! String] {
                // NOTE: assumes response dictionary's values are of same type as local value.
                self.setValue(value, forKey: localKey)
            }
        }
    }
    
    
    /** Property name used to differentiate between other objects; e.g. identifier; is a remote key.
     */
    class func uniquingKey() -> String? {
        return nil
    }
}


// MARK - Core Data 

extension ManagedObject {
   
    class func entityName() -> String? {
        return nil
    }
    
    /** Fetch all items from the store
     */
    class func allItems() -> [AnyObject]? {
        if let entityName = self.entityName() {
            let fetchRequest = NSFetchRequest(entityName:entityName)
            
            do {
                let items = try ManagedStack.sharedInstance.managedObjectContext.executeFetchRequest(fetchRequest)
                
                return items
            } catch let error as NSError {
            }
        }
        return nil
    }
}
