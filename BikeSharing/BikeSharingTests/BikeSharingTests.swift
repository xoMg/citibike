//
//  BikeSharingTests.swift
//  BikeSharingTests
//
//  Created by Alec Montgomery on 7/25/16.
//  Copyright © 2016 Alec Montgomery. All rights reserved.
//

import XCTest
import CoreData
//@testable import BikeSharing

class CoreDataTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testUpdateStationWithRemoteObject() -> () {
        
        var remoteResponse = [
            "capacity": NSNumber(int:0),
            "lat" : NSNumber(double:40.74590996631558),
            "lon" : NSNumber(double:-74.0572714805603),
            "name" : "Coming Soon - Leonard Gordon Park",
            "short_name" : "JC080",
            "station_id" : "3281"
        ]

        let mainContext = ManagedStack.sharedInstance.managedObjectContext

        let entityDescription = NSEntityDescription.entityForName("Station", inManagedObjectContext: mainContext)
        let managedObject = Station(entity: entityDescription!, insertIntoManagedObjectContext: mainContext)
        managedObject.updateObject(remoteResponse)
        
        XCTAssertEqual(managedObject.capacity!, remoteResponse["capacity"])
        XCTAssertEqual(managedObject.latitude!, remoteResponse["lat"])
        XCTAssertEqual(managedObject.longitude!, remoteResponse["lon"])
        XCTAssertEqual(managedObject.name!, remoteResponse["name"])
        XCTAssertEqual(managedObject.shortName!, remoteResponse["short_name"])
        XCTAssertEqual(managedObject.stationID!, remoteResponse["station_id"])
    }
    
    func testUpdateSystemWithRemoteObject() -> () {
        
        let remoteResponse = [
            "data" : [
            
                    "email" : "customerservice@motivateco.com",
                    "language" : "en",
                    "name" : "Citi Bike",
                    "operator" : "Motivate International, Inc.",
                    "phone_number" : "1-855-BIKE-311",
                    "purchase_url" : "http://www.citibikenyc.com/",
                    "short_name" : "Citi Bike",
                    "start_date" : "2013-05-01",
                    "system_id" : "NYC",
                    "timezone" : "America/New_York",
                    "url" : "http://www.citibikenyc.com",
                ],
            "last_updated" : "1469589158",
            "ttl" : "10"]
        
        System.updateObjectsForJSONResponse(remoteResponse, insert: true)
        
        let managedObject = System.allItems()?.first as! System
        
        if let dataDictionary = remoteResponse["data"] {
            if let dictionary = dataDictionary as? NSDictionary {
                
                XCTAssertNotNil(dataDictionary)
            
                let name = dictionary["name"] as! String
                let shortName = dictionary["short_name"] as! String
                let operatorName = dictionary["operator"] as! String
                let language = dictionary["language"] as! String
                let startDate = dictionary["start_date"] as! String
                let systemID = dictionary["system_id"] as! String
                
                XCTAssertEqual(managedObject.name!, name)
                XCTAssertEqual(managedObject.shortName!, shortName)
                XCTAssertEqual(managedObject.operatorName!, operatorName)
                XCTAssertEqual(managedObject.shortName!, shortName)
                XCTAssertEqual(managedObject.language!, language)
                XCTAssertEqual(managedObject.startDate!, startDate)
                XCTAssertEqual(managedObject.systemID!, systemID)
            }
        }
    }
}


class APIClientTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testAPIClientCanMakeRequests() -> () {
        let request = APIRequest(endpoint: "google.com", httpMethod: HTTPMethod.None, params: nil, body: nil)
        
        APIClient.sharedInstance.request(request) { (data, error) in
            XCTAssertNotNil((data == nil && error == nil), "Last Request should not be nil, or Google is down.")
        }
    }
    
    func testAPIClientCanGetStationStatus() {
        
        let request = APIRequest(endpoint: "station_status.json", httpMethod: HTTPMethod.None, params: nil, body: nil)
        
        APIClient.sharedInstance.request(request) { (data, error) in
            XCTAssertNotNil((data == nil && error == nil), "Station Status request should not be nil")
        }
    }
    
    func testAPIClientCanGetStationInformation() {
        
        let request = APIRequest(endpoint: "station_information.json", httpMethod: HTTPMethod.None, params: nil, body: nil)
        
        APIClient.sharedInstance.request(request) { (data, error) in
            XCTAssertNotNil((data == nil && error == nil), "Station information request should not be nil")
        }
    }
    
    func testAPIClientCanGetSystemInformation() {
        
        let request = APIRequest(endpoint: "system_information.json", httpMethod: HTTPMethod.None, params: nil, body: nil)
        
        APIClient.sharedInstance.request(request) { (data, error) in
            XCTAssertNotNil((data == nil && error == nil), "System information request should not be nil")
        }
    }
    
    
}
