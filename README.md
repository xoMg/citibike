#CitiBike Stations

- Swift 2.3 App
- Fetches CitiBike stations and other info from [GBFS] (https://github.com/NABSA/gbfs/blob/master/systems.csv) 
- Displays CitiBike stations on a map
- Uses Core Data as the main persistence mechanism
- Includes unit tests. 


- App is an example of MVC software architecture and many common iOS design patterns. 

------

